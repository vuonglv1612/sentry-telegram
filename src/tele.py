import aiohttp
import ujson
import logging
from tenacity import retry, stop_after_attempt, wait_fixed, before_log

from src.utils import logger


@retry(stop=stop_after_attempt(5), wait=wait_fixed(60), before=before_log(logger, logging.DEBUG), reraise=True)
async def send_message(bot_token, channel_id, message):
    url = f"https://api.telegram.org/bot{bot_token}/sendMessage"
    body = {
        "chat_id": channel_id,
        "text": message,
        "parse_mode": "Markdown"
    }
    async with aiohttp.ClientSession(json_serialize=ujson.dumps) as session:
        async with session.post(url, json=body) as resp:
            return await resp.json()

from fastapi import FastAPI, Body
from src.tele import send_message
from src.config import CommonConfig

app = FastAPI()


@app.post("/sentry")
async def push_tele_message(body = Body(...)):
    str_body = str(body)
    return await send_message(CommonConfig.telegram_bot_token, CommonConfig.telegram_channel_id, str_body)

import os


class CommonConfig:
    sentry_dsn = os.getenv("SENTRY_DSN")
    telegram_bot_token = os.getenv("TELEGRAM_BOT_TOKEN")
    telegram_channel_id = os.getenv("TELEGRAM_CHANNEL_ID")